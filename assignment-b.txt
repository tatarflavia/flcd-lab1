1. Minimum of two numbers
        CODE :
{
// Minimum of two numbers
number a = 3;
number b = 4;
if (a < b) {
    print "a is the minimum of two numbers";
} else {
    print "b is the minimum of two numbers";
};
}
        EXPLANATION : // not fully expanded
Program = CodeBlock {
    CompoundStatement = {
        Comment
        DeclarationStatement
        DeclarationStatement
        IfStatement = if (BinaryExpression) CodeBlock = {
            PrintStatement = print String
        } else CodeBlock = {
            PrintStatement = print String
        }
    }
}

--------------------------------------------------------------------------------

2. Verify if a number is prime
        CODE :
{
number n = 13;
if (n % 2 == 0) {
    print "not prime"
} else {
    number check = 3;
    while (check*check <= n) {
        if (n % check == 0) {
            print "not prime"
            break
        }
        check += 2;
    }
    print "prime"
}
}
        EXPLANATION : // not fully expanded
Program = {
    DeclarationStatement
    IfStatement = if (BinaryExpression =
                        BinaryExpression BinaryOperator Identifier ) {
        PrintStatement
    } else {
        DeclarationStatement
        WhileStatement = while (BinaryExpression =
                                    BinaryExpression BinaryOperator Identifier) {
            if (BinaryExpression =
                    BinaryExpression BinaryOperator Identifier ) {
            PrintStatement
            BreakStatement
            }
            AssignStatement
        }
        PrintStatement
    }
}

--------------------------------------------------------------------------------

3. Compute the average of n numbers
{
number n = read number
number copy = n
number current = 0
number sum = 0
while (n >= 0) {
    n -= 1
    current = read number
    sum += current
}
print "average is "
print (sum / copy)
print " with rest "
print sum % copy
}
